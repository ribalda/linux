#!/bin/bash
# SPDX-License-Identifier: GPL-2.0
set -ve

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd ${DIR}/virtme-tests

rm -f /tmp/test-fail
for s in *.sh; do
	bash $s || touch /tmp/test-fail
done
test ! -f /tmp/test-fail
