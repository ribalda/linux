#!/bin/sh
# SPDX-License-Identifier: GPL-2.0
set -ve

modprobe efivarfs
rmmod efivarfs
modprobe efivarfs
